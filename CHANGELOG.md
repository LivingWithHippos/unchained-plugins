# CHANGELOG

The version is referring to the `"repository_version"` parameter in `repository.json`

### 1.3

- updated plugins for tag matching
- added torrentz2 plugin
- added bt4g plugin
- added disabled field to plugins to allow for disabling plugins without removing them

### 1.2

- updated plugins for new date sorting

### 1.1

- removed atorrents (domain pointing to gambling website)
- updated rlsbb
- added torrentgalaxy

### 1.0

Initial version
